import json
from os import path, getcwd
import requests
import pickle
import sys
import datetime as dt
from optparse import OptionParser

cwd = getcwd()
headers = {'X-Mailgun-Native-Send': 'True'}
def send_message(subject, text):
    return requests.post(
        "https://api.mailgun.net/v3/staging.reporting.colewoodcapital.com/messages",
        auth=("api", "key-dc64f37156456582c9cbf0712f2456f1"),
        headers=headers,
        data={"from": "Autoreports <crypto@staging.reporting.colewoodcapital.com>",
              "to": ["Jamal Cole <jac475@cornell.edu>"],
              "subject": subject,
              "text": text})

def saveState(state, key, symbol, condition, val, time_now):
    try:
        state[key][symbol][condition][val] = time_now
    except KeyError:
        try:
            state[key][symbol][condition] = { val: time_now }
        except KeyError:
            try:
                state[key][symbol] = { condition: { val : time_now }}
            except KeyError:
                state[key] = { symbol : { condition : { val : time_now }}}


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-m', '--mode', help='''POST to price or market table. '''
                                     '''Options = (update_prices, update_market)''')
    parser.add_option('--heartbeat', action='store_true', default=False,
                                help='Sends heartbeat as email message')
    options, _ = parser.parse_args()
    mode = options.mode

    # Open config file
    with open(path.join(cwd, 'config.json')) as f:
        config = json.loads(f.read())

    # Pull data from coinmarketcap web api
    res = requests.request('GET', config['urls']['source'])

    # Reformat data into dict
    original_data = res.json()
    data = {}
    for item in original_data:
        data[item['id']] = item

    # Send POST to server
    if mode == 'update_prices':
        to_update = { 'data': [] }
        for coin in config['update_prices']:
            to_update['data'].append(data[coin])

    if mode == 'update_market':
        to_update = { 'data': original_data }

    res = requests.request('POST', config['urls']['destination'][mode],
             json=to_update)
    res = json.loads(res.text)

    if (res['status'] == 'warning'):   # Add ignored coins to server
        res = requests.request('POST', config['urls']['destination']['update_coins'],
                json={ 'data': original_data })
        res = json.loads(res.text)

    # Check if alert conditions are met
    # Load state (of last time alert was triggered)
    try:
        state = pickle.load(open(path.join(cwd, '.state.pkl'), 'rb'))
    except IOError as err:
        if err.errno == 2: #File does not exist / hasn't been created yet
            state = {}
        else:
            raise(err)

    email_text = ''
    time_now = dt.datetime.now()

    for coin,_ in data.items():
        for key in (x for x in config['alerts'].keys() if x not in ['email', 'reset']):
            for symbol in config['alerts'][key]:
                if symbol in ['any', data[coin]['id']]:
                    for condition, val in list(config['alerts'][key][symbol].items()):
                        try:
                            last_executed = state[key][symbol][condition][val]
                        except KeyError:
                            last_executed = None

                        if (not last_executed) or \
                           (((time_now - last_executed).seconds / 3600.0) >= config['alerts']['reset']):

                            if data[coin][key]:
                                b_condition_met = False

                                if condition == "above":
                                    if float(data[coin][key]) > val:
                                        b_condition_met = True

                                if condition == "below":
                                    if float(data[coin][key]) < val:
                                        b_condition_met = True

                                if b_condition_met:
                                    email_text += "{0} {1}={2} [{3}] condition({4}) met\n".format(
                                        data[coin]['name'], key, data[coin][key], condition, val)

                                    saveState(state, key, symbol, condition, val, time_now)


    # Save state
    with open(path.join(cwd, '.state.pkl'), 'wb') as handle:
        pickle.dump(state, handle)

    # Display output to user
    # print(res)
    # print(email_text)

    # Email message to user
    try:
        message = ''
        if options.heartbeat:
            message += 'heartbeat: True\n'
        if res['status'] != 'success':
            message += json.dumps(res)
        message += email_text
        if message:
            send_message('CoinMarketCap Report [{0}]'.format(mode), message)
    except Exception as e:
        send_message('[ERROR] CoinMarketCap Report', e.message)
        sys.exit(3)

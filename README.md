Script runs only once (i.e. - does not have an event loop); was designed
to be ran by a system process manager (e.g. - systemd or cron)

Set up datebase in server
path/to/server/bin/coinmarketcapcreate
path/to/server/bin/coinmarketcapcreateschema

Modify included config.json to set configuration
alerts: any key from coinmarketcap can be used as an alert (e.g. price_usd,
  percent_change_7d). "any" is a wildcard for all symbols. The following
  commands can be used to check the value of each provided key: above or below.
  'reset' is defined as the number of hours until the same alert will be reset
  if the trigger conditions are still met

To run
$ python3 nolooprun.py

To run with icinga:

$ nano /etc/icinga2/conf.d/commands.conf

object CheckCommand "coinmarketcap" {
  command = [ PluginDir + "/check_coinmarketcap" ]

  arguments = {
    "-m" = "$mode$"
  }
}

$ nano /path/to/coinmarketcap/check_coinmarketcap
... change the absolute path to nolooprun.py if necessary
/projects/tickdatamachine/crypto/coinmarketengine/nolooprun.py

$ ln -s path/to/coinmarketcap/check_coinmarketcap /usr/lib/nagios/plugins/check_coinmarketcap

Test script and initialize database
path/to/coinmarketcap/nolooprun.py -m update_market

sudo service icinga2 restart
